Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
	counter() {
		return Template.instance().counter.get();
	},
});

Template.hello.events({
	'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
	},
});
Template.navbarCentre.events({
	"click .tab-link" (event,instance) {
		if( screen.width > 480 ){event.preventDefault();
		}
		var currentHash = event.target.hash
		var currentTarget = event.target.hash.substring(1)
		history.pushState({target:currentHash}, currentHash, currentHash);
		$(".banner-bg").hide()
		$(".tab-link-mid").hide()
		$(".tab-link-top").hide()
		$(currentHash).show()
	},
});

Template.navbarTop.events({
	"click .tab-link" (event,instance) {
		if( screen.width > 480 ){event.preventDefault();
		}
		var currentHash = event.target.hash
		var currentTarget = event.target.hash.substring(1)
		history.pushState({target:currentHash}, currentHash, currentHash);
		$(".banner-bg").hide()
		$(".tab-link-top").hide()
		$(".tab-link-mid").hide()
		$(currentHash).show()
	},
});

Template.profiles.events({
	"click .tab-link" (event,instance) {
		event.preventDefault();
		var currentHash = event.currentTarget.hash
		var currentTarget = event.currentTarget.hash.substring(1)
		var scrollTo = event.currentTarget.dataset.scroll
		history.pushState({target:currentHash}, currentHash, currentHash);
		$(".banner-bg").hide()
		$(".tab-link-top").hide()
		$(".tab-link-mid").hide()
		$(currentHash).show()
		$(scrollTo).get(0).scrollIntoView();
	},
});

Template.teams.events({
	"click .tab-link" (event,instance) {
		if( screen.width > 480 ){event.preventDefault();
		}
		var currentHash = event.currentTarget.hash
		var currentTarget = event.currentTarget.hash.substring(1)
		var scrollTo = event.currentTarget.dataset.scroll
		history.pushState({target:currentHash}, currentHash, currentHash);
		$(".banner-bg").hide()
		$(".tab-link-top").hide()
		$(".tab-link-mid").hide()
		$(currentHash).show()
		$(scrollTo).get(0).scrollIntoView();
	},
});

$(window).on("popstate", function(e) {
    var currentHash = e.originalEvent.state;
    $(".banner-bg").hide()
	$(".tab-link-top").hide()
	$(".tab-link-mid").hide()
	if (currentHash===null) {
		$("#overview").show()
	}else{
		$(currentHash.target).show()
	}
});